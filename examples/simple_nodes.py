"""
"""
import sys
from flowtest import FlowGraph

graph = FlowGraph()


@graph.step_decorator()
def generate_data():
    return [1, 2, 3]


@graph.step_decorator(generate_data, simple=True)
def verify_sum(data):
    assert sum(data) == 6


if not graph.run_all_flows():
    sys.exit(1)