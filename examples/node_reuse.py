"""
"""
import sys
from flowtest import FlowGraph

graph = FlowGraph()


@graph.step_decorator()
def generate_data():
    return [1, 2, 3]


@graph.step_decorator(generate_data)
def reverse_data(data):
    return reversed(data)


@graph.step_decorator(generate_data, reverse_data)
def verify_sum(data):
    assert sum(data) == 6


if not graph.run_all_flows():
    sys.exit(1)