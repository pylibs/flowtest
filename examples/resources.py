"""
"""
import sys
from tempfile import TemporaryFile

from flowtest import FlowGraph

graph = FlowGraph()


@graph.step_decorator(cleanup=lambda fh: fh.close())
def allocate_file():
    return TemporaryFile()


@graph.step_decorator(allocate_file)
def write_data(fh):
    fh.write(b'Hello World')
    fh.seek(0)
    return fh


@graph.step_decorator(write_data)
def read_data(fh):
    return fh.read()


@graph.step_decorator(read_data)
def verify(text):
    assert text == b'Hello World'


if not graph.run_all_flows():
    sys.exit(1)