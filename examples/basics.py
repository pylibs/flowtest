"""
This example shows the basic principle of flowtest. It defines a test graph with four steps: start, verify_sum,
compute_product and verify_product. The start node generates some data, it does no assertions and therefore this step
should never fail. The verify_sum step takes the data from the start step, computes the sum and verifies (asserts) that
the sum is as expected. The compute_product and verify_product steps do the same for the product of the data numbers,
however here product computation and verification is done in different steps.

Running this graph results in two flows:
Flow 1: start -> verify_sum
Flow 2: start -> compute_product -> verify_product
"""
import operator
from functools import reduce

import sys
from flowtest import FlowGraph

graph = FlowGraph()


@graph.step_decorator()
def start():
    return [1, 2, 3]


@graph.step_decorator(start)
def verify_sum(data):
    assert sum(data) == 6


@graph.step_decorator(start)
def compute_product(data):
    return reduce(operator.mul, data, 1)


@graph.step_decorator(compute_product)
def verify_product(product):
    assert product == 6


if not graph.run_all_flows():
    sys.exit(1)