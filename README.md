This package provides an integration testing framework for Python applications. 

For documentation, see https://flowtest.readthedocs.io/en/latest/
