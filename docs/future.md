# Actions, properties and conditions
The current steps mechanism will be replaced by actions. Actions are also stored in a directed acyclic graph,
but this graph is allowed to contain loops.